Projeto Melhores gifs hackers!!!
 O projeto consiste em uma votação para saber qual Gif hacker é o preferido.

 Para a execução da aplicação será necessario:
  - Ter o python instalado na máquina;
  - Iniciar a virtual env, ex: CaminhoDaVituralEnv.\Scripts\activate.bat
  - Iniciar a aplicaçao com o comando python manage.py runserver
  - Abrir o navegador de sua preferencia e entrar no endereço: 127.0.0.1:8000


 Caso Prefiram, a aplicação esta também online no heroku:
  - Basta acessar o seguinte endereço: daniel-projeto-loeffa.herokuapp.com 

  O projeto foi desenvolvido em Django, em conjunto com a API do tenor. 
  Os gifs são consumidos pela aplicação atraves da api e salvas dentro do banco
  de dados sqlite, com seus respectivos atributos e mostrado aos usuários para 
  que possam votar. 